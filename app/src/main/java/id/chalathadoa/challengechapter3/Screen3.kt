package id.chalathadoa.challengechapter3

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import id.chalathadoa.challengechapter3.data.bio
import id.chalathadoa.challengechapter3.databinding.FragmentScreen3Binding

class Screen3 : Fragment() {
    private var _binding: FragmentScreen3Binding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentScreen3Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tampilNama()
        toFragment4()
//        tampilBiodata()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun tampilNama(){
        binding.usiaAnda.setVisibility(View.GONE)
        binding.alamatAnda.setVisibility(View.GONE)
        binding.pekerjaanAnda.setVisibility(View.GONE)

//        val intent = activity?.intent?.extras
//        binding.namaAnda.text = intent?.getString("nama")

        val args = this.arguments
        val inputName = args?.get("nama")
        binding.namaAnda.text = inputName.toString()
    }

    private fun toFragment4(){
        binding.btnScreen3.setOnClickListener {
            findNavController().navigate(R.id.action_screen3_to_screen4)
        }
    }

    private fun tampilBiodata(){
        val Bio: bio = activity?.intent?.getParcelableExtra("bio")!!
        if (Bio != null){
            binding.usiaAnda.setVisibility(View.VISIBLE)
            binding.alamatAnda.setVisibility(View.VISIBLE)
            binding.pekerjaanAnda.setVisibility(View.VISIBLE)

//        val Bio: bio = activity?.intent?.getParcelableExtra("bio")!!

            if (Bio.usia %2 == 0){
                binding.usiaAnda.text = "Usia anda ${Bio.usia}, Bernilai genap"
            } else {
                binding.usiaAnda.text = "Usia anda ${Bio.usia}, Bernilai ganjil"
            }
            binding.alamatAnda.text = Bio.alamat
            binding.pekerjaanAnda.text = Bio.pekerjaan
        }

//        val args = this.arguments
//        val usia: TextView = binding.usiaAnda
//        val alamat: TextView = binding.alamatAnda
//        val pekerjaan: TextView = binding.pekerjaanAnda
//
//        val inputUsia = args?.getInt("usia")
//        val inputalamat = args?.get("alamat")
//        val inputPekerjaan = args?.get("pekerjaan")
//
//        if (inputUsia != null) {
//            if (inputUsia %2 == 0){
//                usia.text = "Usia anda $inputUsia , Bernilai genap"
//            } else {
//                usia.text = "Usia anda$inputUsia, Bernilai ganjil"
//            }
//        }
//
//        alamat.text = inputalamat.toString()
//        pekerjaan.text = inputPekerjaan.toString()

//        usia.text = inputUsia.toString()
    }
}