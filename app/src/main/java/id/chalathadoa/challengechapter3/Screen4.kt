package id.chalathadoa.challengechapter3

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import id.chalathadoa.challengechapter3.data.bio
import id.chalathadoa.challengechapter3.databinding.FragmentScreen4Binding

class Screen4 : Fragment() {
    private var _binding: FragmentScreen4Binding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentScreen4Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getData()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun getData(){
        binding.btnScreen4.setOnClickListener {
            val usia = binding.etMasukkanUsia.text.toString().toInt()
            val alamat = binding.etMasukkanAlamat.toString()
            val pekerjaan = binding.etMasukkanPekerjaan.toString()

            val Bio = bio(usia, alamat, pekerjaan)
            val intent = Intent(requireContext(), Screen3::class.java)
            intent.putExtra("bio", Bio)
            startActivity(intent)
//            val bundleUsia = Bundle()
//            bundleUsia.putInt("usia", usia)
//            val bundleAlamat = Bundle()
//            bundleAlamat.putString("alamat", alamat)
//            val bundlePekerjaan = Bundle()
//            bundlePekerjaan.putString("pekerjaan", pekerjaan)
//            val fragment = Fragment3()
//            fragment.arguments = bundleUsia
//            fragment.arguments = bundleAlamat
//            fragment.arguments = bundlePekerjaan

            toFragment3()
        }
    }

    private fun toFragment3(){
        findNavController().navigate(R.id.action_screen4_to_screen3)
    }
}