package id.chalathadoa.challengechapter3.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class nama(
    val nama: String
): Parcelable

