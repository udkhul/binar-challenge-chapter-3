package id.chalathadoa.challengechapter3.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class bio(
    val usia: Int,
    val alamat: String?,
    val pekerjaan: String?
): Parcelable
