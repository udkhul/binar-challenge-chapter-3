package id.chalathadoa.challengechapter3

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.navigation.fragment.findNavController
import id.chalathadoa.challengechapter3.databinding.FragmentScreen2Binding

class Screen2 : Fragment() {
    private var _binding: FragmentScreen2Binding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentScreen2Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getName()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun getName(){
        binding.btnScreen2.setOnClickListener {
//            val masukkanNama = binding.masukkanNama.text.toString()
//            val Nama = nama(masukkanNama)
//            val intent = Intent(requireContext(), Fragment3::class.java)
//            intent.putExtra("Nama", Nama)

//            val name = binding.masukkanNama.text.toString()
//            val Nama = nama(name)
//            val intent = Intent(requireContext(), Fragment3::class.java)
//            intent.putExtra("nama", Nama)
//            startActivity(intent)
//
//            val inputNama = binding.masukkanNama.text.toString()
//            val nama = nama(inputNama)
//            val bundle = Bundle()
//            bundle.putString("nama", nama.nama)
//            val intent = Intent(requireContext(), Fragment3::class.java)
//            intent.putExtras(bundle)
//            startActivity(intent)

            val editText: EditText = binding.masukkanNama
            val nama = editText.text.toString()

            val bundle = Bundle()
            bundle.putString("nama", nama)
            val fragment = Screen2()
            fragment.arguments = bundle

            toFragment3()
        }
    }
    private fun toFragment3(){
        findNavController().navigate(R.id.action_screen2_to_screen3)
    }
}