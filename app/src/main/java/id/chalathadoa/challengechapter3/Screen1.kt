package id.chalathadoa.challengechapter3

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import id.chalathadoa.challengechapter3.databinding.FragmentScreen1Binding

class Screen1 : Fragment() {
    private var _binding: FragmentScreen1Binding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentScreen1Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toFragment2()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun toFragment2(){
        binding.btnScreen1.setOnClickListener{
            findNavController().navigate(R.id.action_screen1_to_screen2)
        }
    }
}